# -*- coding: utf-8 -*-
import scrapy


class TruyenfullSpider(scrapy.Spider):
    name = 'truyenfull'
    allowed_domains = ['truyenfull.vn']
    start_urls = ['http://truyenfull.vn/luu-tinh-ho-diep-kiem/']

    def parse(self, response):
        title = response.xpath("//h3[@itemprop='name']//text()").extract_first()
        description = response.xpath("//div[@itemprop='description']//text()")[2].extract()

        raw_chapters = response.xpath("//ul[@class='list-chapter']/li/a")

        found_chapters = []

        for raw_chapter in raw_chapters:
            chapter_name = raw_chapter.css("span::text").extract_first() + raw_chapter.css("a::text").extract_first()
            chapter_url = raw_chapter.css("a::attr(href)").extract_first()

            found_chapters.append({
                "title": chapter_name,
                "url": chapter_url
            })

        data = {
            "title": title,
            "description": description,
            "chapters": found_chapters
        }

        yield data
